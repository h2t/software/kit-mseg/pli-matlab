"""
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::plis
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
"""

import os
import time

import matlab.engine

from mseg import Granularity
from mseg import AlgorithmException, DataHandlingException

import plipython

class SegmentationAlgorithmProxy(plipython.SegmentationAlgorithm):
    
    def __init__(self, base_path):
        # Connect to shared MATLAB engine or create new one
        self.initMatlabEngine()

        # Define PLI in the workspace
        self.pli_instance = self.engine.eval('py.plimatlab.SegmentationAlgorithm()')
        self.engine.workspace['pli_instance'] = self.pli_instance

        # Add MSeg MATLAB libraries to MATLAB path
        matlab_pli_dir = os.envrion.get('MSEG_PLI_MATLAB_DIR', None)
        if matlab_pli_dir is not None: 
            self.engine.addpath(os.path.join(matlab_pli_dir, 'lib'))
        else:
            self.engine.addpath('/usr/share/mseg-pli-matlab/lib')

        # Add project base path to MATLAB path
        self.engine.addpath(self.engine.genpath(base_path))

        # Run setup.m script
        self.engine.setup(nargout = 0)

        # Run constructor.m script
        self.engine.constructor(self.pli_instance, nargout = 0)

        # Get algorithm properties
        self.name = self.engine.char(self.engine.getName(self.pli_instance, nargout = 1))
        self.requiresTraining = self.engine.getRequiresTraining(self.pli_instance, nargout = 1)
        self.trainingGranularity = Granularity.valueOf(self.engine.int64(self.engine.getTrainingGranularity(self.pli_instance, nargout = 1), nargout = 1))

    def train(self):
        try:
            self.engine.train(self.pli_instance, nargout = 0)
        except matlab.engine.MatlabExecutionError as e:
            self.formatExceptionAndRethrow(e)
        except matlab.engine.InterruptedError:
            pass

    def resetTraining(self):
        try:
            self.engine.reset_training(self.pli_instance, nargout = 0)
        except matlab.engine.MatlabExecutionError as e:
            self.formatExceptionAndRethrow(e)
        except matlab.engine.InterruptedError:
            pass

    def segment(self):
        try:
            self.engine.segment(self.pli_instance, nargout = 0)
        except matlab.engine.MatlabExecutionError as e:
            self.formatExceptionAndRethrow(e)
        except matlab.engine.InterruptedError:
            pass

    def internal_getParameters(self, current = None):
        return self.engine.char(self.engine.getParameters(self.pli_instance, nargout = 1), nargout = 1)

    def internal_setParameters(self, json, current = None):
        self.engine.setParameters(self.pli_instance, json.replace('\n', ''), nargout = 0)

    def initMatlabEngine(self):
        print "Initialising MATLAB"
        eng = None

        # If the PLI is quickly restarte, a stray MATLAB engine may be registered
        # But will cause the connection to fail anyway. The loop is a graceful retry
        # for up to 5 tries
        for _ in range(0, 4):
            try:
                eng = matlab.engine.connect_matlab()
                break
            except:
                print 'failed'
                time.sleep(0.1)

        if eng is None:
            raise "Connection failed, abort"

        self.engine = eng

    def formatExceptionAndRethrow(self, e):
        self.engine.eval('exception = MException.last;', nargout = 0)

        if self.engine.eval('isa(exception, \'matlab.exception.PyException\');', nargout = 1):
            eType = self.engine.eval('char(exception.ExceptionObject{1});', nargout = 1)

            if 'mseg.DataHandlingException' in eType:
                msg = self.engine.eval('char(exception.ExceptionObject{2}.reason);', nargout = 1)
                raise DataHandlingException(msg)
            else:
                msg = self.engine.eval('char(exception.ExceptionObject{2});', nargout = 1)
                raise AlgorithmException(msg)
        else:
            msg = self.engine.eval('exception.message;', nargout = 1).strip()

            size = int(self.engine.eval('size(exception.stack, 1);', nargout = 1))
            isTruncated = size > 5
            size = min(size, 5)

            for i in range(1, size + 1):
                stack = self.engine.eval('exception.stack(' + str(i) + ');', nargout = 1)
                msg = msg + "\n\n" + 'File: ' + stack['file'] + "\n" + 'Line: ' + str(int(stack['line'])) + "\n" + 'Name: ' + stack['name']

            if isTruncated:
                msg = msg + "\n\n" + '...'
                msg = msg + "\n\n" + 'For more information see the terminal output'

            raise AlgorithmException(msg)
