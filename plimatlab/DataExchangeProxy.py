"""
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MSeg::plis
 * @author     Nicklas Kulp ( nicklas-kulp at gmx dot de )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
"""

import time
import os

import Ice

from mseg import DataExchangeInterfacePrx

from plipython import DataExchangeProxy as DataExchangeProxyBase

class DataExchangeProxy(DataExchangeProxyBase):

    def __init__(self):

        self.ic = None
        self.data = None

    def __del__(self):

        try:

            self.ic.destroy()

        except:

            pass
        
    def connect(self):

        home = os.environ['HOME']
        parameters = ['--Ice.Config=' + home + '/.armarx/default.cfg']

        self.ic = Ice.initialize(parameters)

        de = self.ic.stringToProxy('DataExchange')
        self.data = DataExchangeInterfacePrx.checkedCast(de)
